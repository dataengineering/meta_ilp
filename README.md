# meta ilp

Download the MovieLens 20M Dataset,
http://files.grouplens.org/datasets/movielens/ml-20m.zip


Split the users into users with 2000 or more ratings,
1000 - 1999 rating, and fewer than 1000 ratings:

  cat ratings.csv | cut -d ',' -f 1 | uniq -c | sort -rn |\
     sed 's/ *//' | sed 's/ /,/'    > ratings.counts

   grep '[2-9]...,' < ratings.counts > ratings.2k_or_more.csv
   grep '[1]...,' < ratings.counts > ratings.1k.csv
   grep '^...,' < ratings.counts > ratings.few.csv
   grep '^..,' < ratings.counts >> ratings.few.csv
   grep '^.,' < ratings.counts >> ratings.few.csv


The earliest timestap is 1000000065, so convert all timestamps into
days since 1000000000 to compare against recently seen movies:

  cat ratings.csv | gawk -F, ' {T = int(($4 - 1000000000)/60/60/24) ; print $1 "," $2 "," $3 "," T}' > ratings_days.csv


For experiments looking to compare rating date to movie release date, convert
all timestamps into YYYY_MM_DD:

  cat ratings.csv | gawk -F, ' {D = strftime("%C%y_%m_%d",$4,1) ; print $1 "," $2 "," $3 "," D}' > ratings_date.csv


Split the ratings of the 255 members of >=2k group into separate
experiments, dropping the userid since they are in separate files:

cut -d ',' -f 2  ratings.2k_or_more.csv | while read userid ; do grep ^$userid ratings_date.csv | cut -d ',' -f 2-4 > more_than_2k/ratings.$userid.csv ; done

Join on the first field (the movieID) to get the TMDB id:
sort -k1,1 -t',' links.csv > links_sorted.csv
for f in more_than_2k/* ; do sort -k1,1 -t',' $f | join -t',' -a1 - links_sorted.csv | cut -d',' -f 2,3,5 > more_than_2k_tmdb/${f#more_than_2k/} ; done

